package com.nisrinalya.suitmediamobileassessment.data.model;

import java.io.Serializable;
import java.util.List;

public class GetEventResponse implements Serializable {
    private boolean status;
    private List<GetEvent> data;
    private String message;

    public GetEventResponse(boolean status, List<GetEvent> data, String message) {
        this.status = status;
        this.data = data;
        this.message = message;
    }

    public boolean isStatus() {
        return status;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }

    public List<GetEvent> getData() {
        return data;
    }

    public void setData(List<GetEvent> data) {
        this.data = data;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
