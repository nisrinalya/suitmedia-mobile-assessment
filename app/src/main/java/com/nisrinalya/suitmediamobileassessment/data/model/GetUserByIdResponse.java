package com.nisrinalya.suitmediamobileassessment.data.model;

import java.io.Serializable;

public class GetUserByIdResponse implements Serializable {
    private boolean status;
    private GetUser user;
    private String message;

    public GetUserByIdResponse(boolean status, GetUser user, String message) {
        this.status = status;
        this.user = user;
        this.message = message;
    }

    public boolean isStatus() {
        return status;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }

    public GetUser getUser() {
        return user;
    }

    public void setUser(GetUser user) {
        this.user = user;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
