package com.nisrinalya.suitmediamobileassessment.data.model;

import java.io.Serializable;

public class GetEvent implements Serializable {
    private String id;
    private String name;
    private String desc;
    private double lat;
    private double lng;
    private String created;

    public GetEvent(String id, String name, String desc, double lat, double lng, String created) {
        this.id = id;
        this.name = name;
        this.desc = desc;
        this.lat = lat;
        this.lng = lng;
        this.created = created;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    public double getLat() {
        return lat;
    }

    public void setLat(double lat) {
        this.lat = lat;
    }

    public double getLng() {
        return lng;
    }

    public void setLng(double lng) {
        this.lng = lng;
    }

    public String getCreated() {
        return created;
    }

    public void setCreated(String created) {
        this.created = created;
    }
}
