package com.nisrinalya.suitmediamobileassessment.data;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import androidx.annotation.Nullable;

import com.nisrinalya.suitmediamobileassessment.data.model.AddUserResponse;
import com.nisrinalya.suitmediamobileassessment.data.model.DefaultResponse;
import com.nisrinalya.suitmediamobileassessment.data.model.GetEvent;
import com.nisrinalya.suitmediamobileassessment.data.model.GetEventResponse;
import com.nisrinalya.suitmediamobileassessment.data.model.GetUser;
import com.nisrinalya.suitmediamobileassessment.data.model.GetUserByIdResponse;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;

public class DBHelper extends SQLiteOpenHelper {
    public static final String DB_NAME = "suitmedia";
    public static final int DB_VERSION = 1;

    /* user */
    public static final String TB_USER = "user";

    public static final String KEY_USER_ID = "id";
    public static final String KEY_USER_NAME = "name";
    public static final String KEY_USER_IMAGE = "image";
    public static final String KEY_USER_CREATED = "created";
    public static final String KEY_USER_UPDATED = "updated";

    /* guest */
    public static final String TB_GUEST = "guest";

    public static final String KEY_GUEST_ID = "id";
    public static final String KEY_GUEST_EMAIL = "email";
    public static final String KEY_GUEST_FIRST_NAME = "first_name";
    public static final String KET_GUEST_LAST_NAME = "last_name";
    public static final String KEY_GUEST_AVATAR = "avatar";
    public static final String KEY_GUEST_CREATED = "created";
    public static final String KEY_GUEST_UPDATED = "updated";

    /* event */
    public static final String TB_EVENT = "event";

    public static final String KEY_EVENT_ID = "id";
    public static final String KEY_EVENT_NAME = "name";
    public static final String KEY_EVENT_DESC = "description";
    public static final String KEY_EVENT_LATITUDE = "latitude";
    public static final String KEY_EVENT_LONGITUDE = "longitude";
    public static final String KEY_EVENT_CREATED = "created";
    public static final String KEY_EVENT_UPDATED = "updated";

    public DBHelper(@Nullable Context context) {
        super(context, DB_NAME, null, DB_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {
        /* user */
        sqLiteDatabase.execSQL(
                "CREATE TABLE " + TB_USER + "("
                + KEY_USER_ID + " INTEGER PRIMARY KEY AUTOINCREMENT,"
                + KEY_USER_NAME + " TEXT,"
                + KEY_USER_IMAGE + " BLOB DEFAULT X'00',"
                + KEY_USER_CREATED + " TEXT,"
                + KEY_USER_UPDATED + " TEXT);"
        );

        /* guest */
        sqLiteDatabase.execSQL(
                "CREATE TABLE " + TB_GUEST + "("
                + KEY_GUEST_ID + " INTEGER,"
                + KEY_GUEST_EMAIL + " TEXT,"
                + KEY_GUEST_FIRST_NAME + " TEXT,"
                + KET_GUEST_LAST_NAME + " TEXT,"
                + KEY_GUEST_AVATAR + " TEXT,"
                + KEY_GUEST_CREATED + " TEXT,"
                + KEY_GUEST_UPDATED + " TEXT);"
        );

        /* event */
        sqLiteDatabase.execSQL(
                "CREATE TABLE " + TB_EVENT + "("
                + KEY_EVENT_ID + " INTEGER PRIMARY KEY AUTOINCREMENT,"
                + KEY_EVENT_NAME + " TEXT,"
                + KEY_EVENT_DESC + " TEXT,"
                + KEY_EVENT_LATITUDE + " REAL,"
                + KEY_EVENT_LONGITUDE + " REAL,"
                + KEY_EVENT_CREATED + " TEXT,"
                + KEY_EVENT_UPDATED + " TEXT);"
        );
    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i1) {
        sqLiteDatabase.execSQL("DROP TABLE IF EXISTS " + TB_USER);
        sqLiteDatabase.execSQL("DROP TABLE IF EXISTS " + TB_GUEST);
        sqLiteDatabase.execSQL("DROP TABLE IF EXISTS " + TB_EVENT);

        onCreate(sqLiteDatabase);
    }

    public AddUserResponse addUser(String name, byte[] image) {
        SQLiteDatabase db = getWritableDatabase();
        ContentValues values = new ContentValues();

        values.put(KEY_USER_NAME, name);
        values.put(KEY_USER_IMAGE, image);
        values.put(KEY_USER_CREATED, LocalDateTime.now().format(DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss")));
        values.put(KEY_USER_UPDATED, LocalDateTime.now().format(DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss")));

        try {
            long id = db.insertWithOnConflict(
                    TB_USER,
                    null,
                    values,
                    SQLiteDatabase.CONFLICT_IGNORE
            );

            return new AddUserResponse(
                    true,
                    Long.toString(id),
                    "User Successfully Added"
            );
        } catch (SQLiteException e) {
            return new AddUserResponse(
                    false,
                    null,
                    "Failed to Add User: " + e.getMessage()
            );
        }
    }

    public GetUserByIdResponse getUserById(long id) {
        SQLiteDatabase db = getReadableDatabase();

        try {
            Cursor cursor = db.rawQuery(
                    "SELECT * FROM " + TB_USER
                    + " WHERE " + KEY_USER_ID + "=" + id + ";",
                    null
            );

            GetUser getUser = null;
            if (cursor.getCount() != 0) {
                cursor.moveToFirst();

                getUser = new GetUser(
                        cursor.getString(cursor.getColumnIndexOrThrow(KEY_USER_ID)),
                        cursor.getString(cursor.getColumnIndexOrThrow(KEY_USER_NAME)),
                        cursor.getBlob(cursor.getColumnIndexOrThrow(KEY_USER_IMAGE))
                );
            }

            return new GetUserByIdResponse(
                    true,
                    getUser,
                    "OK"
            );
        } catch (SQLiteException e) {
            return new GetUserByIdResponse(
                    true,
                    null,
                    e.getMessage()
            );
        }
    }

    public DefaultResponse addGuest(
            int id,
            String email,
            String firstName,
            String lastName,
            String avatar
    ) {
        SQLiteDatabase db = getWritableDatabase();
        ContentValues values = new ContentValues();

        values.put(KEY_GUEST_ID, id);
        values.put(KEY_GUEST_EMAIL, email);
        values.put(KEY_GUEST_FIRST_NAME, firstName);
        values.put(KET_GUEST_LAST_NAME, lastName);
        values.put(KEY_GUEST_AVATAR, avatar);

        try {
            boolean isGuestAvailable = checkGuestAvailability(id);
            if (isGuestAvailable) {
                return new DefaultResponse(
                        true,
                        "Data available"
                );
            } else {
                db.insertWithOnConflict(
                        TB_GUEST,
                        null,
                        values,
                        SQLiteDatabase.CONFLICT_IGNORE
                );

                return new DefaultResponse(
                        true,
                        "Data added"
                );
            }
        } catch (SQLiteException e) {
            return new DefaultResponse(
                    false,
                    e.getMessage()
            );
        }
    }

    private boolean checkGuestAvailability(int id) {
        SQLiteDatabase db = getReadableDatabase();
        boolean isAvailable;

        try {
            Cursor cursor = db.rawQuery(
                    "SELECT COUNT(*) AS total FROM " + TB_GUEST
                    + " WHERE " + KEY_GUEST_ID + "=" + id + ",",
                    null
            );

            int total = cursor.getInt(cursor.getColumnIndexOrThrow("total"));
            isAvailable = total != 0;
        } catch (SQLiteException e) {
            Log.i("GuestAvailException", e.getMessage());
            isAvailable = false;
        }

        return isAvailable;
    }

    public DefaultResponse addEvent(
            String name,
            String desc,
            double lat,
            double lng
    ) {
        SQLiteDatabase db = getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(KEY_EVENT_NAME, name);
        values.put(KEY_EVENT_DESC, desc);
        values.put(KEY_EVENT_LATITUDE, lat);
        values.put(KEY_EVENT_LONGITUDE, lng);
        values.put(KEY_EVENT_CREATED, LocalDateTime.now().format(DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss")));
        values.put(KEY_EVENT_UPDATED, LocalDateTime.now().format(DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss")));

        try {
            db.insertWithOnConflict(
                    TB_EVENT,
                    null,
                    values,
                    SQLiteDatabase.CONFLICT_IGNORE
            );

            return new DefaultResponse(
                    true,
                    "Event added"
            );
        } catch (SQLiteException e) {
            return new DefaultResponse(
                    false,
                    "Failed to add event: " + e.getMessage()
            );
        }
    }

    public GetEventResponse getAllEvents() {
        SQLiteDatabase db = getReadableDatabase();

        try {
            ArrayList<GetEvent> eventList = new ArrayList<>();

            Cursor cursor = db.rawQuery(
                    "SELECT * FROM " + TB_EVENT
                    + " ORDER BY " + KEY_EVENT_UPDATED + " DESC;",
                    null
            );

            if (cursor.moveToFirst()) {
                do {
                    eventList.add(
                            new GetEvent(
                                    cursor.getString(cursor.getColumnIndexOrThrow(KEY_EVENT_ID)),
                                    cursor.getString(cursor.getColumnIndexOrThrow(KEY_EVENT_NAME)),
                                    cursor.getString(cursor.getColumnIndexOrThrow(KEY_EVENT_DESC)),
                                    cursor.getDouble(cursor.getColumnIndexOrThrow(KEY_EVENT_LATITUDE)),
                                    cursor.getDouble(cursor.getColumnIndexOrThrow(KEY_EVENT_LONGITUDE)),
                                    cursor.getString(cursor.getColumnIndexOrThrow(KEY_EVENT_CREATED))
                            )
                    );
                } while (cursor.moveToNext());
            }

            return new GetEventResponse(
                    true,
                    eventList,
                    "OK"
            );
        } catch (SQLiteException e) {
            return new GetEventResponse(
                    false,
                    null,
                    "Data not found"
            );
        }
    }
}
