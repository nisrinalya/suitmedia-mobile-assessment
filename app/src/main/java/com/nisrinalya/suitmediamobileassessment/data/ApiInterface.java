package com.nisrinalya.suitmediamobileassessment.data;

import com.nisrinalya.suitmediamobileassessment.data.model.GetGuestResponse;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface ApiInterface {
    @GET("users")
    Call<GetGuestResponse> getGuests(
            @Query("page") int page,
            @Query("per_page") int pageSize
    );
}
