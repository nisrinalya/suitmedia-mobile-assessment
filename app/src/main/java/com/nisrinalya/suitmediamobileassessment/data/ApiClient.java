package com.nisrinalya.suitmediamobileassessment.data;

public class ApiClient {
    public static final String BASE_URL = "https://reqres.in/api/";

    public static ApiInterface getApiInterface() {
        return RetrofitClient.getClient(BASE_URL)
                .create(ApiInterface.class);
    }
}
