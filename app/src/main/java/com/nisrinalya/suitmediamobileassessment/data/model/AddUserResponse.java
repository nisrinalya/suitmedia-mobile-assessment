package com.nisrinalya.suitmediamobileassessment.data.model;

import java.io.Serializable;

public class AddUserResponse implements Serializable {
    private boolean status;
    private String id;
    private String message;

    public AddUserResponse(boolean status, String id, String message) {
        this.status = status;
        this.id = id;
        this.message = message;
    }

    public boolean isStatus() {
        return status;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
