package com.nisrinalya.suitmediamobileassessment.ui.adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.recyclerview.widget.RecyclerView;

import com.nisrinalya.suitmediamobileassessment.R;
import com.nisrinalya.suitmediamobileassessment.data.model.GetGuest;
import com.squareup.picasso.Picasso;

import java.util.List;

public class GuestAdapter extends RecyclerView.Adapter<GuestAdapter.ViewHolder> {
    private final List<GetGuest> guestList;

    public GuestAdapter(List<GetGuest> guestList) {
        this.guestList = guestList;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_guest, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        final GetGuest currGuest = guestList.get(position);

        if (currGuest.getAvatar() != null && !currGuest.getAvatar().isEmpty()) {
            Picasso.get()
                    .load(currGuest.getAvatar())
                    .into(holder.image);
        }

        holder.name.setText(
                String.format(
                        "%s %s",
                        currGuest.getFirstName(),
                        currGuest.getLastName()
                )
        );
    }

    @Override
    public int getItemCount() {
        return guestList.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        AppCompatImageView image;
        AppCompatTextView name;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);

            image = itemView.findViewById(R.id.image);
            name = itemView.findViewById(R.id.name);
        }
    }
}
