package com.nisrinalya.suitmediamobileassessment.ui;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import android.Manifest;
import android.content.ComponentName;
import android.content.ContentResolver;
import android.content.ContentValues;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.content.res.ColorStateList;
import android.graphics.Color;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.Parcelable;
import android.provider.MediaStore;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import com.google.android.material.card.MaterialCardView;
import com.google.android.material.textfield.TextInputEditText;
import com.google.android.material.textfield.TextInputLayout;
import com.nisrinalya.suitmediamobileassessment.R;
import com.nisrinalya.suitmediamobileassessment.data.DBHelper;
import com.nisrinalya.suitmediamobileassessment.data.model.AddUserResponse;
import com.nisrinalya.suitmediamobileassessment.data.model.DefaultResponse;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {
    MaterialCardView btnSelectImg;
    MaterialCardView btnNext;
    MaterialCardView btnCheck;

    AppCompatImageView image;
    AppCompatImageView imageDefault;
    TextInputLayout inputNameContainer;
    TextInputLayout inputPalindromeContainer;
    TextInputEditText inputName;
    TextInputEditText inputPalindrome;

    private Uri output;
    private Uri selectedImage = null;

    private DBHelper dbHelper;

    public static final int REQUEST_SELECT_IMAGE = 1001;
    public static final int REQUEST_READ_EXTERNAL = 1002;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        dbHelper = new DBHelper(this);

        btnSelectImg = findViewById(R.id.image_container);
        btnNext = findViewById(R.id.btn_next);
        btnCheck = findViewById(R.id.btn_check);

        image = findViewById(R.id.image);
        imageDefault = findViewById(R.id.image_default);
        inputNameContainer = findViewById(R.id.input_name_container);
        inputPalindromeContainer = findViewById(R.id.input_palindrome_container);
        inputName = findViewById(R.id.input_name);
        inputPalindrome = findViewById(R.id.input_palindrome);

        btnSelectImg.setOnClickListener(view -> {
            if (ContextCompat.checkSelfPermission(this, Manifest.permission.READ_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED) {
                selectImage();
            } else {
                ActivityCompat.requestPermissions(
                        this,
                        new String[]{ Manifest.permission.READ_EXTERNAL_STORAGE },
                        REQUEST_READ_EXTERNAL
                );
            }
        });

        btnNext.setOnClickListener(view -> {
            if (inputName.getText().toString().isEmpty() || inputPalindrome.getText().toString().isEmpty()) {
                checkErrorName();
                checkErrorPalindrome();
            } else {
                byte[] bytesImage = null;
                if (selectedImage != null) {
                    bytesImage = getBytesImage();
                }

                AddUserResponse response = dbHelper.addUser(
                        inputName.getText().toString(),
                        bytesImage
                );

                if (response.isStatus()) {
                    Toast.makeText(this, response.getMessage(), Toast.LENGTH_SHORT).show();

                    Intent intent = new Intent(this, WelcomeActivity.class);
                    intent.putExtra(WelcomeActivity.EXTRA_USER_ID, response.getId());

                    startActivity(intent);
                    finish();
                } else {
                    Toast.makeText(this, response.getMessage(), Toast.LENGTH_SHORT).show();
                }
            }
        });

        btnCheck.setOnClickListener(view -> {
            if (inputPalindrome.getText().toString().isEmpty()) {
                checkErrorPalindrome();
            } else {
                String text = inputPalindrome.getText().toString();

                StringBuilder reversedText = new StringBuilder();
                reversedText.append(text);
                reversedText.reverse();

                if (text.equalsIgnoreCase(reversedText.toString())) {
                    inputPalindromeContainer.setEndIconDrawable(R.drawable.ic_round_check);
                    inputPalindromeContainer.setEndIconTintList(ColorStateList.valueOf(Color.parseColor("#43A047")));
                } else {
                    inputPalindromeContainer.setEndIconDrawable(R.drawable.ic_round_close);
                    inputPalindromeContainer.setEndIconTintList(ColorStateList.valueOf(Color.parseColor("#E53935")));
                }
                inputPalindromeContainer.setEndIconMode(TextInputLayout.END_ICON_CUSTOM);
            }
        });

        inputName.addTextChangedListener(onName());
        inputPalindrome.addTextChangedListener(onPalindrome());

        updateImageView();
    }

    private byte[] getBytesImage() {
        ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
        InputStream inputStream;
        try {
            inputStream = getContentResolver().openInputStream(selectedImage);
            byte[] buff = new byte[1024];
            int i;

            while (-1 != (i = inputStream.read(buff))) {
                outputStream.write(buff, 0, i);
            }

            return outputStream.toByteArray();
        } catch (Exception e) {
            Log.e("BytesImageException", e.getMessage());
            return null;
        }
    }

    private TextWatcher onPalindrome() {
        return new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                checkErrorPalindrome();
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        };
    }

    private TextWatcher onName() {
        return new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                checkErrorName();
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        };
    }

    private void checkErrorPalindrome() {
        if (inputPalindrome.getText().toString().isEmpty()) {
            inputPalindromeContainer.setErrorEnabled(true);
            inputPalindromeContainer.setError("This field cannot be empty");
        } else {
            inputPalindromeContainer.setErrorEnabled(false);
            inputPalindromeContainer.setError(null);
        }

        inputPalindromeContainer.setEndIconDrawable(null);
    }

    private void checkErrorName() {
        if (inputName.getText().toString().isEmpty()) {
            inputNameContainer.setErrorEnabled(true);
            inputNameContainer.setError("This field cannot be empty");
        } else {
            inputNameContainer.setErrorEnabled(false);
            inputNameContainer.setError(null);
        }
    }

    private void updateImageView() {
        if (selectedImage == null) {
            imageDefault.setVisibility(View.VISIBLE);
            image.setVisibility(View.GONE);
        } else {
            imageDefault.setVisibility(View.GONE);
            image.setVisibility(View.VISIBLE);

            image.setImageURI(selectedImage);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        if (requestCode == REQUEST_READ_EXTERNAL) {
            if (grantResults.length > 0) {
                if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    selectImage();
                } else if (grantResults[0] == PackageManager.PERMISSION_DENIED) {
                    Toast.makeText(this, "Can't select image", Toast.LENGTH_SHORT).show();
                }
            }
        }
    }

    private void selectImage() {
        String relativePath = Environment.DIRECTORY_DOWNLOADS;
        String filename = "suitmedia_mobile_" + LocalDateTime.now().format(DateTimeFormatter.ofPattern("yyyyMMddHHmmssSSS")) + ".jpg";
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.Q) {
            ContentValues values = new ContentValues();
            values.put(MediaStore.MediaColumns.DISPLAY_NAME, filename);
            values.put(MediaStore.MediaColumns.RELATIVE_PATH, relativePath);

            ContentResolver cr = getContentResolver();
            output = cr.insert(MediaStore.Downloads.EXTERNAL_CONTENT_URI, values);
        } else {
            String path = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS).toString();
            File file = new File(path, filename);
            output = Uri.fromFile(file);
        }

        List<Intent> camIntents = new ArrayList<>();
        Intent capIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        PackageManager pm = getPackageManager();
        List<ResolveInfo> camList = pm.queryIntentActivities(capIntent, 0);
        for (ResolveInfo info : camList) {
            String packageName = info.activityInfo.packageName;

            Intent intent = new Intent(capIntent);
            intent.setComponent(
                    new ComponentName(
                            packageName,
                            info.activityInfo.name
                    )
            );
            intent.setPackage(packageName);
            intent.putExtra(MediaStore.EXTRA_OUTPUT, output);

            camIntents.add(intent);
        }

        Intent galleryIntent = new Intent();
        galleryIntent.setType("image/*");
        galleryIntent.setAction(Intent.ACTION_GET_CONTENT);

        Intent chooserIntent = Intent.createChooser(galleryIntent, "Select Image");
        chooserIntent.putExtra(
                Intent.EXTRA_INITIAL_INTENTS,
                camIntents.toArray(new Parcelable[camIntents.size()])
        );

        startActivityForResult(chooserIntent, REQUEST_SELECT_IMAGE);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == REQUEST_SELECT_IMAGE) {
            if (resultCode == RESULT_OK) {
                boolean isCamera;

                if (data == null) {
                    isCamera = true;
                } else {
                    String action = data.getAction();
                    if (action == null) {
                        isCamera = false;
                    } else {
                        isCamera = action.equals(MediaStore.ACTION_IMAGE_CAPTURE);
                    }
                }

                if (isCamera) {
                    selectedImage = output;
                } else {
                    selectedImage = data.getData();
                }

                updateImageView();
            }
        }
    }
}