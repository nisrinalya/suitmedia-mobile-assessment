package com.nisrinalya.suitmediamobileassessment.ui;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.AppCompatTextView;

import android.content.Intent;
import android.os.Bundle;

import com.google.android.material.card.MaterialCardView;
import com.nisrinalya.suitmediamobileassessment.R;
import com.nisrinalya.suitmediamobileassessment.data.DBHelper;
import com.nisrinalya.suitmediamobileassessment.data.model.GetUser;
import com.nisrinalya.suitmediamobileassessment.data.model.GetUserByIdResponse;

public class WelcomeActivity extends AppCompatActivity {
    MaterialCardView btnEvent;
    MaterialCardView btnGuest;

    AppCompatTextView username;

    private DBHelper dbHelper;

    public static final String EXTRA_USER_ID = "extra_user_id";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_welcome);

        dbHelper = new DBHelper(this);

        btnEvent = findViewById(R.id.btn_event);
        btnGuest = findViewById(R.id.btn_guest);

        username = findViewById(R.id.username);

        String userId = getIntent().getStringExtra(EXTRA_USER_ID);
        loadUserData(Long.parseLong(userId));

        btnGuest.setOnClickListener(view -> {
            Intent intent = new Intent(this, GuestActivity.class);
            startActivity(intent);
        });

        btnEvent.setOnClickListener(view -> {
            Intent intent = new Intent(this, EventActivity.class);
            startActivity(intent);
        });
    }

    private void loadUserData(long userId) {
        GetUserByIdResponse response = dbHelper.getUserById(userId);
        if (response.isStatus()) {
            GetUser user = response.getUser();
            username.setText(user.getName());
        }
    }
}