package com.nisrinalya.suitmediamobileassessment.ui;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.widget.NestedScrollView;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewTreeObserver;
import android.widget.ProgressBar;

import com.nisrinalya.suitmediamobileassessment.R;
import com.nisrinalya.suitmediamobileassessment.data.ApiClient;
import com.nisrinalya.suitmediamobileassessment.data.ApiInterface;
import com.nisrinalya.suitmediamobileassessment.data.DBHelper;
import com.nisrinalya.suitmediamobileassessment.data.model.DefaultResponse;
import com.nisrinalya.suitmediamobileassessment.data.model.GetGuest;
import com.nisrinalya.suitmediamobileassessment.data.model.GetGuestResponse;
import com.nisrinalya.suitmediamobileassessment.ui.adapter.GuestAdapter;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class GuestActivity extends AppCompatActivity {
    Toolbar toolbar;
    RecyclerView recyclerItem;
    NestedScrollView container;
    ProgressBar progressBar;

    private final ArrayList<GetGuest> guestList = new ArrayList<>();
    private GuestAdapter adapterItem;

    private int page = 1;

    private boolean isFirst = true;

    private DBHelper dbHelper;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_guest);

        dbHelper = new DBHelper(this);

        toolbar = findViewById(R.id.toolbar);
        recyclerItem = findViewById(R.id.content);
        container = findViewById(R.id.container);
        progressBar = findViewById(R.id.progress);

        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        /* set adapter to recyclerview */
        adapterItem = new GuestAdapter(guestList);
        recyclerItem.setAdapter(adapterItem);

        /* get data from API */
        loadData();
    }

    private void loadData() {
        progressBar.setVisibility(isFirst ? View.GONE : View.VISIBLE);

        ApiInterface apiInterface = ApiClient.getApiInterface();
        apiInterface.getGuests(page, 10).enqueue(new Callback<GetGuestResponse>() {
            @Override
            public void onResponse(Call<GetGuestResponse> call, Response<GetGuestResponse> response) {
                progressBar.setVisibility(View.GONE);

                if (response.isSuccessful()) {
                    int totalPage = response.body().getTotalPages();
                    List<GetGuest> guests = response.body().getData();
                    if (guests != null && !guests.isEmpty()) {
                        if (isFirst) {
                            guestList.clear();
                            isFirst = false;
                        }

                        guestList.addAll(guests);
                        adapterItem.notifyDataSetChanged();

                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                            guests.forEach(g -> addToLocal(g));
                        } else {
                            for (GetGuest g : guests) {
                                addToLocal(g);
                            }
                        }

                        /* set pagination */
                        container.getViewTreeObserver().addOnScrollChangedListener(() -> {
                            View view = container.getChildAt(container.getChildCount() - 1);
                            int diff = view.getBottom() - (container.getHeight() + container.getScrollY());

                            if (diff == 0 && totalPage > page) {
                                page++;
                                loadData();
                            }
                        });
                    }
                }
            }

            @Override
            public void onFailure(Call<GetGuestResponse> call, Throwable t) {
                progressBar.setVisibility(View.GONE);
            }
        });
    }

    private void addToLocal(GetGuest guest) {
        DefaultResponse response = dbHelper.addGuest(
                guest.getId(),
                guest.getEmail(),
                guest.getFirstName(),
                guest.getLastName(),
                guest.getAvatar()
        );

        Log.i("GuestToLocal", response.getMessage());
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            onBackPressed();
        }

        return super.onOptionsItemSelected(item);
    }
}