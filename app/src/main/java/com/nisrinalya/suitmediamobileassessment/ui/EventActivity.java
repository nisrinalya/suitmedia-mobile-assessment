package com.nisrinalya.suitmediamobileassessment.ui;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.content.IntentSender;
import android.content.pm.PackageManager;
import android.location.Location;
import android.os.Bundle;
import android.os.Looper;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.common.api.ApiException;
import com.google.android.gms.common.api.ResolvableApiException;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationCallback;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationResult;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.LocationSettingsResponse;
import com.google.android.gms.location.LocationSettingsStatusCodes;
import com.google.android.gms.location.Priority;
import com.google.android.gms.location.SettingsClient;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.tasks.Task;
import com.google.android.material.textfield.TextInputEditText;
import com.google.android.material.textfield.TextInputLayout;
import com.nisrinalya.suitmediamobileassessment.R;
import com.nisrinalya.suitmediamobileassessment.data.DBHelper;
import com.nisrinalya.suitmediamobileassessment.data.model.DefaultResponse;

public class EventActivity extends AppCompatActivity implements OnMapReadyCallback, GoogleMap.OnMarkerClickListener {
    Toolbar toolbar;
    View container;
    SupportMapFragment mapFragment;

    private boolean isMaps = false;
    private FusedLocationProviderClient fusedLocationClient;
    private GoogleMap googleMap;

    private DBHelper dbHelper;

    public static final int REQUEST_LOCATION_PERMISSION = 2001;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_event);

        fusedLocationClient = LocationServices.getFusedLocationProviderClient(this);
        dbHelper = new DBHelper(this);

        toolbar = findViewById(R.id.toolbar);
        container = findViewById(R.id.container);

        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        mapFragment = (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.maps);
        mapFragment.getMapAsync(this);
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            onBackPressed();
        } else if (item.getItemId() == R.id.action_maps) {
            // TODO: 28/06/2022 switch to maps
            updateMenuUI();
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.event_menu, menu);

        updateMenuUI();

        return super.onCreateOptionsMenu(menu);
    }

    private void updateMenuUI() {
        if (isMaps) {
            toolbar.getMenu().findItem(R.id.action_maps).setIcon(R.drawable.ic_round_format_list_bulleted);
            isMaps = false;
        } else {
            toolbar.getMenu().findItem(R.id.action_maps).setIcon(R.drawable.ic_round_map);
            isMaps = true;
        }
    }

    @Override
    public void onMapReady(@NonNull GoogleMap googleMap) {
        this.googleMap = googleMap;

        getLongLat();

        googleMap.setOnMapLongClickListener(latLng -> googleMap.addMarker(new MarkerOptions().position(latLng)));
        googleMap.setOnMarkerClickListener(this::onMarkerClick);
    }

    private void showDialogAddEvent(Marker marker) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getApplicationContext());
        View view = LayoutInflater.from(getApplicationContext()).inflate(
                R.layout.dialog_add_event,
                findViewById(R.id.add_event)
        );

        builder.setView(view);
        builder.setCancelable(false);

        TextInputLayout nameContainer = view.findViewById(R.id.input_name_container);
        TextInputEditText name = view.findViewById(R.id.input_name);
        TextInputEditText desc = view.findViewById(R.id.input_desc);
        TextView btnSave = view.findViewById(R.id.btn_save);

        AlertDialog alertDialog = builder.create();

        name.addTextChangedListener(onName(nameContainer, name));

        btnSave.setOnClickListener(v -> {
            if (name.getText().toString().isEmpty()) {
                checkErrorName(nameContainer, name);
            } else {
                DefaultResponse response = dbHelper.addEvent(
                        name.getText().toString(),
                        desc.getText().toString(),
                        marker.getPosition().latitude,
                        marker.getPosition().longitude
                );

                if (response.isStatus()) {
                    alertDialog.dismiss();
                    finish();
                }

                Toast.makeText(this, response.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });

        alertDialog.show();
    }

    private TextWatcher onName(TextInputLayout nameContainer, TextInputEditText name) {
        return new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                checkErrorName(nameContainer, name);
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        };
    }

    private void checkErrorName(TextInputLayout nameContainer, TextInputEditText name) {
        if (name.getText().toString().isEmpty()) {
            nameContainer.setErrorEnabled(true);
            nameContainer.setError("This field cannot be empty");
        } else {
            nameContainer.setErrorEnabled(false);
            nameContainer.setError(null);
        }
    }

    private void getLongLat() {
        LocationRequest locationRequest = LocationRequest.create();
        locationRequest.setInterval(10000);
        locationRequest.setFastestInterval(5000);
        locationRequest.setPriority(Priority.PRIORITY_HIGH_ACCURACY);

        LocationSettingsRequest.Builder builder = new LocationSettingsRequest.Builder().addLocationRequest(locationRequest);
        SettingsClient client = LocationServices.getSettingsClient(this);
        Task<LocationSettingsResponse> responseTask = client.checkLocationSettings(builder.build());

        responseTask.addOnCompleteListener(task -> {
            try {
                LocationSettingsResponse response = responseTask.getResult(ApiException.class);
                if (response.getLocationSettingsStates().isLocationPresent()) {
                    enableLocation();
                }
            } catch (ApiException e) {
                switch (e.getStatusCode()) {
                    case LocationSettingsStatusCodes.RESOLUTION_REQUIRED:
                        try {
                            ResolvableApiException resolvable = (ResolvableApiException) e;
                            resolvable.startResolutionForResult(
                                    this,
                                    51
                            );
                        } catch (IntentSender.SendIntentException | ClassCastException ex) {
                            ex.printStackTrace();
                        }
                        break;
                    case LocationSettingsStatusCodes.SETTINGS_CHANGE_UNAVAILABLE:
                        Toast.makeText(this, "Location Setting not available.", Toast.LENGTH_SHORT).show();
                        break;
                }
            }
        });
    }

    @SuppressLint("MissingPermission")
    private void enableLocation() {
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
            fusedLocationClient.getLastLocation().addOnCompleteListener(task -> {
                Location location = task.getResult();
                if (location != null) {
                    LatLng currLocation = new LatLng(location.getLatitude(), location.getLongitude());
//                    googleMap.addMarker(new MarkerOptions().position(currLocation));
                    googleMap.moveCamera(CameraUpdateFactory.newLatLngZoom(currLocation, 18f));
                } else {
                    requestNewLocation();
                }
            });
        } else {
            ActivityCompat.requestPermissions(this,
                    new String[]{
                            Manifest.permission.ACCESS_COARSE_LOCATION,
                            Manifest.permission.ACCESS_FINE_LOCATION
                    }, REQUEST_LOCATION_PERMISSION);
        }
    }

    @SuppressLint("MissingPermission")
    private void requestNewLocation() {
        LocationRequest locationRequest = new LocationRequest();

        locationRequest.setPriority(Priority.PRIORITY_HIGH_ACCURACY);
        locationRequest.setInterval(5);
        locationRequest.setFastestInterval(0);
        locationRequest.setNumUpdates(1);

        fusedLocationClient = LocationServices.getFusedLocationProviderClient(this);
        fusedLocationClient.requestLocationUpdates(locationRequest, locationCallback, Looper.myLooper());
    }

    private final LocationCallback locationCallback = new LocationCallback() {
        @Override
        public void onLocationResult(@NonNull LocationResult locationResult) {
            Location location = locationResult.getLastLocation();

            LatLng currLocation = new LatLng(location.getLatitude(), location.getLongitude());
//            googleMap.addMarker(new MarkerOptions().position(currLocation));
            googleMap.moveCamera(CameraUpdateFactory.newLatLngZoom(currLocation, 18f));
        }
    };

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        if (requestCode == REQUEST_LOCATION_PERMISSION) {
            if (grantResults.length > 0) {
                if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    enableLocation();
                } else if (grantResults[0] == PackageManager.PERMISSION_DENIED){
                    Toast.makeText(this, "Can't get location", Toast.LENGTH_SHORT).show();
                }
            }
        }
    }

    @Override
    public boolean onMarkerClick(@NonNull Marker marker) {
        showDialogAddEvent(marker);
        return false;
    }
}